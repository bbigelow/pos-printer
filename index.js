var app = require('application');


module.exports = {
	requestConnect: function () {
		var context = app.android.context;
		var interface_usb = new POSAPI.POSUSBAPI(context);

		var pos_sdk = new POSSDK.POSSDK(interface_usb);

		var error_code = interface_usb.OpenDevice();

		interface_usb.CloseDevice();
	},
	openDrawer: function () {
		var context = app.android.context;
		var interface_usb = new POSAPI.POSUSBAPI(context);

		var pos_sdk = new POSSDK.POSSDK(interface_usb);

		var error_code = interface_usb.OpenDevice();

		error_code = pos_sdk.cashdrawerOpen(0, 100, 100);
		if (error_code != 1000) {
			this.logErrorCode('drawer open 0: ', error_code);
			error_code = null;
		}
		error_code = pos_sdk.cashdrawerOpen(1, 100, 100);
		if (error_code != 1000) {
			error_code = null;
		}

		interface_usb.CloseDevice();
	},
	printReceipt: function (ticketDetails) {

		var context = app.android.context;
		var interface_usb = new POSAPI.POSUSBAPI(context);

		var pos_sdk = new POSSDK.POSSDK(interface_usb);

		var error_code = interface_usb.OpenDevice();
		this.logErrorCode('Open Device', error_code);

		error_code = pos_sdk.systemSelectPrintMode(0);
		this.logErrorCode('Select Print Mode', error_code);
		error_code = pos_sdk.standardModeSetStartingPosition(5);
		this.logErrorCode('Set Start Position', error_code);
		error_code = pos_sdk.textStandardModeAlignment(1);
		error_code = pos_sdk.textSetLineHeight(10);
		this.logErrorCode('Set Line Height', error_code);
		error_code = pos_sdk.textSelectFontMagnifyTimes(2, 2);
		this.logErrorCode('Set Magnify Times', error_code);
		this.logErrorCode('Setting title alignment', error_code);
		var title = new java.lang.String(`${ticketDetails.company.name}\n`);
		var titleBuffer = title.getBytes("GB18030");
		error_code = pos_sdk.textPrint(titleBuffer, titleBuffer.length);
		this.logErrorCode('Text Print Title', error_code);

        //sub header
        error_code = pos_sdk.textStandardModeAlignment(1);
		this.logErrorCode('Set alignment', error_code);
        error_code = pos_sdk.textSetLineHeight(2);
		this.logErrorCode('Set line height', error_code);
        error_code = pos_sdk.textSelectFontMagnifyTimes(1, 1);
		this.logErrorCode('Set text size', error_code);
		error_code = pos_sdk.systemFeedLine(15);
		this.logErrorCode('feed line', error_code);


        var subHeader = new java.lang.String(`${ticketDetails.company.phoneNumber}\n`)
        var subHeaderBuffer = subHeader.getBytes("GB18030");
        error_code = pos_sdk.textPrint(subHeaderBuffer, subHeaderBuffer.length);
		this.logErrorCode('Set alignment', error_code);


		//Line Items
		error_code = pos_sdk.textStandardModeAlignment(2);
		this.logErrorCode('Setting line item alignment', error_code);
		error_code = pos_sdk.systemFeedLine(20);
		this.logErrorCode('feed line from title', error_code);

		var items = ticketDetails.lineItems;

		items.forEach(function (item) {
			var lineItem =
				new java.lang.String(`${item.name}\t${item.quantity} @ $${Number(item.cost).toFixed(2)}\t$${Number((item.cost * item.quantity)).toFixed(2)}\n`);
			var lineItemBuffer = lineItem.getBytes('GB18030');
			error_code = pos_sdk.textPrint(lineItemBuffer, lineItemBuffer.length);
		});
		this.logErrorCode('Text print line item', error_code);

		error_code = pos_sdk.systemFeedLine(30);
		this.logErrorCode('After line item feed', error_code);

		//Totals
		error_code = pos_sdk.textStandardModeAlignment(2);
		this.logErrorCode('Setting totals alignment', error_code);
		var totaling = new java.lang.String(`Subtotal\t$${Number(ticketDetails.subTotal).toFixed(2)}\nTax\t\t$${ticketDetails.taxAmount}\nTotal\t\t$${ticketDetails.total}\n\n`);
		var totalingBuffer = totaling.getBytes('GB18030');
		error_code = pos_sdk.textPrint(totalingBuffer, totalingBuffer.length);
		this.logErrorCode('print totals', error_code);
		var paidAndChange = new java.lang.String(`Paid\t\t$${Number(ticketDetails.paid).toFixed(2)}\nChange Due\t$${Number(ticketDetails.change).toFixed(2)}\n\n\n\n\n`);
		var paidAndChangeBuffer = paidAndChange.getBytes('GB18030');
		error_code = pos_sdk.textPrint(paidAndChangeBuffer, paidAndChangeBuffer.length);
		this.logErrorCode('print text', error_code);

		error_code = pos_sdk.systemFeedLine(30);
		this.logErrorCode('feed line', error_code);

		error_code = pos_sdk.textStandardModeAlignment(0);
		this.logErrorCode('set align', error_code);

		var sig = new java.lang.String(`Signature: x___________________________`);
		var sigBuffer = sig.getBytes('GB18030');
		error_code = pos_sdk.textPrint(sigBuffer, sigBuffer.length);
		this.logErrorCode('Print sig', error_code);


		this.logErrorCode('print paid and change', error_code);
		error_code = pos_sdk.systemFeedLine(255);
		this.logErrorCode('feed line', error_code);

		error_code = interface_usb.CloseDevice();
		this.logErrorCode('Close device', error_code);
	},
	logErrorCode: function (action, code) {
		if (code != 1000 && code != null) {
			console.error(action + '\nCode: ' + code);
		}
	}
}